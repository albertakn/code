#include <algorithm>
#include <iostream>
#include <limits>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <vector>

typedef std::pair<int, int> Position;
const int desk_size = 8;
const int number_of_fields = 64;
const Position kw = std::make_pair(3, 3);
const std::vector<std::pair<Position, Position>> term_states{
    std::make_pair(Position(2, 2), Position(1, 1)),
    std::make_pair(Position(2, 2), Position(2, 1)),
    std::make_pair(Position(3, 2), Position(3, 1)),
    std::make_pair(Position(1, 1), Position(3, 1)),
    std::make_pair(Position(5, 1), Position(3, 1)),
    std::make_pair(Position(6, 1), Position(3, 1)),
    std::make_pair(Position(7, 1), Position(3, 1)),
    std::make_pair(Position(8, 1), Position(3, 1)),
    std::make_pair(Position(4, 2), Position(4, 1)),
    std::make_pair(Position(2, 2), Position(1, 2)),
    std::make_pair(Position(2, 3), Position(1, 3)),
    std::make_pair(Position(1, 1), Position(1, 3)),
    std::make_pair(Position(1, 5), Position(1, 3)),
    std::make_pair(Position(1, 6), Position(1, 3)),
    std::make_pair(Position(1, 7), Position(1, 3)),
    std::make_pair(Position(1, 8), Position(1, 3)),
    std::make_pair(Position(2, 4), Position(1, 4)),
    std::make_pair(Position(1, 6), Position(1, 4)),
    std::make_pair(Position(6, 1), Position(4, 1))};
const int inf = std::numeric_limits<int>::max() - 1;
const std::vector<std::pair<int, int>> directions{
    std::make_pair(0, 1),  std::make_pair(-1, 0),  std::make_pair(1, 1),
    std::make_pair(1, -1), std::make_pair(-1, -1), std::make_pair(0, -1),
    std::make_pair(1, 0),  std::make_pair(-1, 1)};

bool IsCorrectPos(const Position &position) {
  return (position.first >= 1 && position.first <= desk_size &&
          position.second >= 1 && position.second <= desk_size);
}

bool IsKingBeats(const Position &k, const Position &position) {
  for (auto direction : directions) {
    Position move_pos(k.first + direction.first, k.second + direction.second);
    if (move_pos.first == position.first &&
        move_pos.second == position.second) {
      return true;
    }
  }
  return false;
}

bool IsQueenBeats(const Position &qw, const Position &position) {
  if (qw == position) {
    return false;
  }
  for (auto direction : directions) {
    for (int i = 1; i <= desk_size; i++) {
      Position move_pos(qw.first + direction.first * i,
                        qw.second + direction.second * i);
      if (!IsCorrectPos(move_pos) || move_pos == kw) {
        break;
      }
      if (move_pos == position) {
        return true;
      }
    }
  }
  return false;
}

struct State {
  State(Position qw, Position kb, bool white_turn)
      : qw_(qw), kb_(kb), white_turn_(white_turn) {}

  int GetStateNumber() const {
    return ((white_turn_ ? 0 : number_of_fields * number_of_fields) +
            number_of_fields * (desk_size * (kb_.first - 1) + kb_.second - 1) +
            desk_size * (qw_.first - 1) + qw_.second - 1);
  }

  bool Check() const { return IsKingBeats(kw, kb_) || IsQueenBeats(qw_, kb_); }

  bool operator<(const State &state) const {
    return this->GetStateNumber() - state.GetStateNumber() < 0;
  }

  Position qw_;
  Position kb_;
  bool white_turn_;
};

class Endgame {
 public:
  Endgame() {
    for (int qw_x = 1; qw_x <= desk_size; qw_x++) {
      for (int qw_y = 1; qw_y <= desk_size; qw_y++) {
        for (int kb_x = 1; kb_x <= desk_size; kb_x++) {
          for (int kb_y = 1; kb_y <= desk_size; kb_y++) {
            State desk1(Position(qw_x, qw_y), Position(kb_x, kb_y), true);
            states_.emplace(desk1, inf);
            State desk2(Position(qw_x, qw_y), Position(kb_x, kb_y), false);
            states_.emplace((desk2), inf);
          }
        }
      }
    }
    for (auto pair : term_states) {
      states_[State(pair.first, pair.second, false)] = 0;
    }
    for (auto pair : states_) {
      AddWhiteStates(pair.first);
    }
  }

  int Solver(State start) {
    while (!queue_.empty() && states_[start] == inf) {
      State cur_state = queue_.front();
      queue_.pop();
      if (cur_state.white_turn_) {
        if (cur_state.Check()) {
          continue;
        }
        int min_moves = FindMinMoves(cur_state);
        if (states_[cur_state] > min_moves + 1) {
          states_[cur_state] = min_moves + 1;
          AddBlackStates(cur_state);
        }
      } else {
        int max_moves = FindMaxMoves(cur_state);
        if (max_moves != inf) {
          if (states_[cur_state] == inf || states_[cur_state] < max_moves + 1) {
            states_[cur_state] = max_moves + 1;
            AddWhiteStates(cur_state);
          }
        }
      }
    }
    return states_[start];
  }

 private:
  int FindMinMoves(State cur_state) {
    int min_moves = inf;
    for (auto direction : directions) {
      for (int i = 1; i <= desk_size; i++) {
        Position position(cur_state.qw_.first + direction.first * i,
                          cur_state.qw_.second + direction.second * i);
        if (!IsCorrectPos(position) || position == cur_state.kb_ ||
            position == kw) {
          break;
        }
        State new_state(position, cur_state.kb_, !cur_state.white_turn_);
        if (IsKingBeats(cur_state.kb_, position) &&
            !IsKingBeats(kw, position)) {
          continue;
        }
        min_moves = std::min(min_moves, states_[new_state]);
      }
    }
    return min_moves;
  }

  int FindMaxMoves(State cur_state) {
    int max_moves = -inf;
    for (auto direction : directions) {
      Position position(cur_state.kb_.first + direction.first,
                        cur_state.kb_.second + direction.second);
      State new_state(cur_state.qw_, position, !cur_state.white_turn_);
      if (!IsCorrectPos(position) || position == cur_state.qw_ ||
          position == kw || new_state.Check()) {
        continue;
      }
      max_moves = std::max(max_moves, states_[new_state]);
    }
    return max_moves;
  }

  void AddWhiteStates(State state) {
    if (state.white_turn_) {
      return;
    }
    for (auto direction : directions) {
      for (int i = 1; i <= desk_size; i++) {
        Position position(state.qw_.first + direction.first * i,
                          state.qw_.second + direction.second * i);
        if (!IsCorrectPos(position) || position == state.kb_ ||
            position == kw) {
          break;
        }
        State new_desk(Position(position.first, position.second), state.kb_,
                       !state.white_turn_);
        if (new_desk.Check() || states_[new_desk] != inf) {
          continue;
        }
        queue_.push(new_desk);
      }
    }
  }

  void AddBlackStates(State state) {
    if (!state.white_turn_) {
      return;
    }
    for (auto direction : directions) {
      Position position(state.kb_.first + direction.first,
                        state.kb_.second + direction.second);
      State new_desk(state.qw_, position, !state.white_turn_);
      if (!IsCorrectPos(position) || position == state.qw_ || position == kw ||
          IsKingBeats(kw, position) || states_[new_desk] != inf) {
        continue;
      }
      queue_.push(new_desk);
    }
  }
  std::map<State, int> states_;
  std::queue<State> queue_;
};

int main() {
  char qw_x;
  char qw_y;
  char kb_x;
  char kb_y;
  Endgame solve;
  std::cin >> qw_x >> qw_y >> kb_x >> kb_y;
  State start(Position(qw_x - 'a' + 1, qw_y - '0'),
              Position(kb_x - 'a' + 1, kb_y - '0'), true);
  int result = solve.Solver(start);
  if (result != inf) {
    std::cout << result;
  } else {
    std::cout << "IMPOSSIBLE";
  }
  return 0;
}
