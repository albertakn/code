#include <iostream>
#include <string>
#include <vector>

class KMPSearcher {
 public:
  explicit KMPSearcher(const std::string &pattern) : pattern_(pattern) {
    pi_.resize(pattern.size(), 0);

    for (size_t i = 1; i < pattern.size(); ++i) {
      pi_[i] = GetPrefixFunction(pattern[i], pi_[i - 1]);
    }
  }

  template <class Callback>
  void Search(const std::string &text, Callback on_occurence) {
    size_t last_prefix_value = 0;
    for (size_t i = 0; i < text.size(); ++i) {
      last_prefix_value = GetPrefixFunction(text[i], last_prefix_value);
      if (last_prefix_value == pattern_.size()) {
        on_occurence(i + 1 - pattern_.size());
      }
    }
  }

 private:
  size_t GetPrefixFunction(char symbol, size_t last_prefix_value) {
    size_t index = last_prefix_value;
    while (index > 0 && symbol != pattern_[index]) {
      index = pi_[index - 1];
    }
    if (symbol == pattern_[index]) {
      return index + 1;
    }
    return 0;
  }

  const std::string &pattern_;
  std::vector<size_t> pi_;
};

int main() {
  std::string pattern_string;
  std::string text_string;
  std::cin >> pattern_string;
  std::cin >> text_string;
  KMPSearcher kmp(pattern_string);
  kmp.Search(text_string, [](size_t i) { std::cout << i << " "; });
  return 0;
}
