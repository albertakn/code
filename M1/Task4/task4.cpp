#include <algorithm>
#include <deque>
#include <iostream>
#include <map>
#include <memory>
#include <queue>
#include <unordered_map>
#include <vector>

struct AhoCorasickNode {
  std::vector<size_t> terminal_ids_;

  std::map<char, AhoCorasickNode> trie_transitions_;

  std::unordered_map<char, AhoCorasickNode *> automaton_transitions_cache_;

  AhoCorasickNode *suffix_link_ = nullptr;

  AhoCorasickNode *terminal_link_ = nullptr;

  bool AutomationTransitionFound(char ch) const {
    auto iter = automaton_transitions_cache_.find(ch);
    return iter != automaton_transitions_cache_.cend();
  }

  bool TrieTransitionFound(char ch) const {
    auto iter = trie_transitions_.find(ch);
    return iter != trie_transitions_.cend();
  }
};

AhoCorasickNode *GetAutomatonTransition(AhoCorasickNode *node, char ch) {
  if (node->AutomationTransitionFound(ch)) {
    return node->automaton_transitions_cache_[ch];
  }
  if (node->suffix_link_ != nullptr) {
    AhoCorasickNode *node_by_suffix_link =
        GetAutomatonTransition(node->suffix_link_, ch);
    node->automaton_transitions_cache_[ch] = node_by_suffix_link;
    return node_by_suffix_link;
  }
  return node;
}

class NodeReference {
 public:
  NodeReference() = default;

  explicit NodeReference(AhoCorasickNode *node) : node_(node) {}

  NodeReference Next(char ch) const {
    return NodeReference(GetAutomatonTransition(node_, ch));
  }

  template <class Callback>
  void ForEachMatch(Callback callback) const {
    auto cur_node = node_;
    while (cur_node != nullptr) {
      for (auto ids : cur_node->terminal_ids_) {
        callback(ids);
      }
      cur_node = cur_node->terminal_link_;
    }
  }

 private:
  AhoCorasickNode *node_ = nullptr;
};

class AhoCorasick {
 public:
  AhoCorasick() = default;

  AhoCorasick(const AhoCorasick &) = delete;

  AhoCorasick &operator=(const AhoCorasick &) = delete;

  AhoCorasick(AhoCorasick &&) = delete;

  AhoCorasick &operator=(AhoCorasick &&) = delete;

  NodeReference Root() const { return NodeReference(&root_); }

 private:
  friend class AhoCorasickBuilder;

  mutable AhoCorasickNode root_;
};

class AhoCorasickBuilder {
 public:
  void AddString(std::string string, size_t id) {
    strings_.push_back(std::move(string));
    ids_.push_back(id);
  }

  std::unique_ptr<AhoCorasick> Build() {
    auto automaton = std::make_unique<AhoCorasick>();
    for (size_t i = 0; i < strings_.size(); ++i) {
      AddString(&automaton->root_, strings_[i], ids_[i]);
    }
    CalculateLinks(&automaton->root_);
    return automaton;
  }

 private:
  static void AddString(AhoCorasickNode *root, const std::string &string,
                        size_t id) {
    AhoCorasickNode *current_node = root;
    for (const char c : string) {
      if (!(current_node->TrieTransitionFound(c))) {
        current_node->trie_transitions_[c] = AhoCorasickNode();
      }
      current_node->automaton_transitions_cache_[c] =
          &(current_node->trie_transitions_[c]);
      current_node = &(current_node->trie_transitions_[c]);
    }
    current_node->terminal_ids_.push_back(id);
  }

  static void CalculateLinks(AhoCorasickNode *root) {
    std::queue<AhoCorasickNode *> queue;
    queue.push(root);
    while (!queue.empty()) {
      auto current_state = queue.front();
      queue.pop();
      for (auto &child : current_state->trie_transitions_) {
        queue.push(&(child.second));
        char symbol = child.first;
        auto temp_node = current_state->suffix_link_;

        bool is_suffix_link_set = false;
        while (temp_node != nullptr && !is_suffix_link_set) {
          if (temp_node->TrieTransitionFound(symbol)) {
            child.second.suffix_link_ = &(temp_node->trie_transitions_[symbol]);
            is_suffix_link_set = true;
          } else {
            temp_node = temp_node->suffix_link_;
          }
        }

        if (!is_suffix_link_set) {
          child.second.suffix_link_ = root;
        }

        if (child.second.suffix_link_ != root) {
          if (child.second.suffix_link_->terminal_ids_.empty()) {
            child.second.terminal_link_ =
                child.second.suffix_link_->terminal_link_;
          } else {
            child.second.terminal_link_ = child.second.suffix_link_;
          }
        } else {
          child.second.terminal_link_ = nullptr;
        }
      }
    }
  }

  std::vector<std::string> strings_;
  std::vector<size_t> ids_;
};

void Split(char delimiter, const std::string &string, std::vector<size_t> &ids,
           std::vector<std::string> &patterns) {
  size_t index = 0;
  size_t start_of_pattern = 0;

  while (index < string.size()) {
    while (index < string.size() && string[index] == delimiter) {
      ++index;
    }
    if (start_of_pattern != index) {
      start_of_pattern = index;
    }
    while (index < string.size() && string[index] != delimiter) {
      ++index;
    }
    if (start_of_pattern != index) {
      patterns.emplace_back(
          string.substr(start_of_pattern, index - start_of_pattern));
      ids.emplace_back(index);
      start_of_pattern = index;
    }
  }
}

class WildcardMatcher {
 public:
  WildcardMatcher(const std::string &pattern, char wildcard) {
    AhoCorasickBuilder trie;
    std::vector<size_t> ids;
    std::vector<std::string> patterns;
    Split(wildcard, pattern, ids, patterns);
    for (size_t i = 0; i < patterns.size(); ++i) {
      trie.AddString(patterns[i], ids[i]);
    }
    automaton_ = trie.Build();
    state_ = automaton_->Root();
    number_of_words_ = ids.size();
    pattern_length_ = pattern.size();
    occurrences_by_offset_.resize(pattern.size(), 0);
  }

  template <class Callback>
  void Scan(char character, size_t index, Callback on_match) {
    state_ = state_.Next(character);
    UpdateWordOccurrencesCounters();
    if (occurrences_by_offset_.front() == number_of_words_ &&
        index >= pattern_length_ - 1) {
      on_match();
    }
    ShiftWordOccurrencesCounters();
  }

 private:
  void UpdateWordOccurrencesCounters() {
    state_.ForEachMatch(
        [&](size_t i) { ++occurrences_by_offset_[pattern_length_ - i]; });
  }

  void ShiftWordOccurrencesCounters() {
    occurrences_by_offset_.pop_front();
    occurrences_by_offset_.push_back(0);
  }

  std::deque<size_t> occurrences_by_offset_;
  size_t number_of_words_;
  size_t pattern_length_;
  std::unique_ptr<AhoCorasick> automaton_;
  NodeReference state_;
};

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::string pattern;
  std::cin >> pattern;
  size_t pattern_size = pattern.size();
  WildcardMatcher matcher = WildcardMatcher(pattern, '?');
  char symbol;
  size_t position = 0;
  while (std::cin >> symbol) {
    matcher.Scan(symbol, position, [&]() {
      (std::cout << (position + 1) - pattern_size << ' ');
    });
    ++position;
  }
}
