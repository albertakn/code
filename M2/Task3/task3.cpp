#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

class SuffixArray {
 public:
  SuffixArray(const std::string &string, size_t alphabet_size)
      : string_(string), alphabet_size_(alphabet_size) {
    string_ += char(0);
    string_size_ = string_.size();
    suffix_array_.resize(string_size_);
    classes_.resize(string_size_);
  }

  void BuildSuffixArray() {
    size_t classes = ZeroStep();
    std::vector<size_t> temporary_suffix_array(string_size_);
    std::vector<size_t> temporary_classes(string_size_);
    for (size_t cur_leng = 2; cur_leng < 2 * string_size_; cur_leng *= 2) {
      size_t prev_leng = cur_leng / 2;
      for (size_t i = 0; i < string_size_; ++i) {
        temporary_suffix_array[i] =
            ((string_size_ + suffix_array_[i]) - prev_leng) % string_size_;
      }
      std::vector<size_t> vector_for_counting_sort(classes);
      for (int i = 0; i < string_size_; ++i) {
        ++vector_for_counting_sort[classes_[temporary_suffix_array[i]]];
      }
      for (size_t i = 1; i < classes; ++i) {
        vector_for_counting_sort[i] += vector_for_counting_sort[i - 1];
      }
      for (int i = string_size_ - 1; i >= 0; --i) {
        suffix_array_
            [--vector_for_counting_sort[classes_[temporary_suffix_array[i]]]] =
                temporary_suffix_array[i];
      }
      size_t new_num_classes = 1;
      temporary_classes[suffix_array_[0]] = 0;
      for (size_t i = 1; i < string_size_; ++i) {
        size_t first_value =
            classes_[(suffix_array_[i] + prev_leng) % string_size_];
        size_t second_value =
            classes_[(suffix_array_[i - 1] + prev_leng) % string_size_];
        if (classes_[suffix_array_[i]] != classes_[suffix_array_[i - 1]] ||
            (first_value != second_value)) {
          ++new_num_classes;
        }
        temporary_classes[suffix_array_[i]] = new_num_classes - 1;
      }

      classes_ = temporary_classes;
      classes = new_num_classes;
    }
  }
  // lcp[i] = LCP[s[suff[i], s[suff[i + 1]]
  void BuildLcp() {
    position_.resize(string_size_);
    lcp_.resize(string_size_);

    for (size_t i = 0; i < string_size_; ++i) {
      position_[suffix_array_[i]] = i;
    }

    size_t current_lcp_size = 0;
    for (size_t i = 0; i < string_size_; ++i) {
      if (current_lcp_size > 0) {
        --current_lcp_size;
      }
      if (position_[i] == string_size_ - 1) {
        lcp_[string_size_ - 1] = -1;
        current_lcp_size = 0;
        continue;
      }
      size_t j = suffix_array_[position_[i] + 1];
      while (std::max(i + current_lcp_size, j + current_lcp_size) <
                 string_size_ &&
             string_[i + current_lcp_size] == string_[j + current_lcp_size]) {
        ++current_lcp_size;
      }
      lcp_[position_[i]] = current_lcp_size;
    }
  }
  size_t GetStringSize() { return string_size_ - 1; }

  size_t Getlcp(size_t i) { return lcp_[i]; }

  size_t GetKthSuffix(size_t k) { return suffix_array_[k]; }

 private:
  size_t ZeroStep() {
    std::vector<size_t> vector_for_counting_sort(alphabet_size_);
    for (size_t index = 0; index < string_size_; ++index) {
      ++vector_for_counting_sort[string_[index]];
    }
    for (size_t index = 1; index < alphabet_size_; ++index) {
      vector_for_counting_sort[index] += vector_for_counting_sort[index - 1];
    }
    for (size_t index = 0; index < string_size_; ++index) {
      suffix_array_[--vector_for_counting_sort[string_[index]]] = index;
    }
    size_t classes = 1;
    classes_[suffix_array_[0]] = 0;
    for (size_t i = 1; i < string_size_; ++i) {
      if (string_[suffix_array_[i - 1]] != string_[suffix_array_[i]]) {
        ++classes;
      }
      classes_[suffix_array_[i]] = classes - 1;
    }
    return classes;
  }

  std::vector<size_t> lcp_;
  size_t string_size_;
  std::string string_;
  std::vector<size_t> position_;  // array inverse to suf array
  std::vector<size_t> suffix_array_;
  std::vector<size_t> classes_;
  size_t alphabet_size_;
};

bool IsDifferent(size_t first, size_t second, size_t s1_len) {
  return (first < s1_len && second > s1_len) ||
         (first > s1_len && second < s1_len);
}

void FindCummonOrdinalStatistic(long long k, std::string &first_string,
                                std::string &second_string) {
  size_t first_string_size = first_string.size();
  const size_t alphabite_size = 256;
  std::string general_string = first_string + char(1) + second_string;
  SuffixArray suffix_array = SuffixArray(general_string, alphabite_size);
  suffix_array.BuildSuffixArray();
  suffix_array.BuildLcp();
  size_t first_index;
  size_t size;

  long long cntr = 0;
  size_t last_lcp = 0;
  for (int i = 2; i < suffix_array.GetStringSize(); ++i) {
    if (IsDifferent(suffix_array.GetKthSuffix(i),
                    suffix_array.GetKthSuffix(i + 1), first_string_size)) {
      cntr +=
          (suffix_array.Getlcp(i) - std::min(last_lcp, suffix_array.Getlcp(i)));
      if (cntr >= k) {
        first_index = suffix_array.GetKthSuffix(i);
        size = suffix_array.Getlcp(i) - (cntr - k);
        break;
      }
      last_lcp = suffix_array.Getlcp(i);
    } else {
      last_lcp = std::min(last_lcp, suffix_array.Getlcp(i));
    }
  }

  if (cntr < k) {
    std::cout << -1;
  } else {
    for (size_t i = first_index; i < first_index + size; ++i) {
      std::cout << general_string[i];
    }
  }
}

int main() {
  std::string first_string;
  std::string second_string;
  long long k;
  std::cin >> first_string;
  std::cin >> second_string;
  std::cin >> k;
  FindCummonOrdinalStatistic(k, first_string, second_string);
  return 0;
}
